public class UserProfile {

    private String first_name;
    private String last_name;
    private String email_address;

    public UserProfile(String first, String last, String email){
        first_name = first;
        last_name = last;
        email_address = email;
    }

    public String getFirst_name(){
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail_address() {
        return email_address;
    }
    
    public String getFullName() {
    	return first_name + " " + last_name;
    }

}