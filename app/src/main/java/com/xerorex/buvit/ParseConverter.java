package com.xerorex.buvit;

/**
 * Created by LAViATHoR on 11/29/2015.
 */
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Binder;
import android.util.Log;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import com.parse.ParseObject;

public class ParseConverter extends Service implements DataConverter<ParseObject, UserProfile>  {

    private final IBinder converterBinder = new MyLocalConverterBinder();

    @Override

    //Converts an array of ParseObjects to an ArrayList of UserProfiles. 

    public void arrayToList(ParseObject[] array, List<UserProfile> list) {

        for(ParseObject i :array)
            list.add(convertToOutput(i));

    }

    @Override
    public void listToArray(List<ParseObject> list, UserProfile[] array) {

        for(int i = 0; i < list.size(); i++)
            array[i]= convertToOutput(list.get(i));

    }

    public void listToArrayList(List<ParseObject> list, ArrayList<UserProfile> alist){

        for(ParseObject i: list)
            alist.add(convertToOutput(i));
    }

    public void userArrayToUserArrayList(UserProfile[] array, ArrayList<UserProfile> list){

        for(UserProfile i : array)
            list.add(i);

    }

    @Override
    public UserProfile convertToOutput(ParseObject x) {

        UserProfile temp = new UserProfile();

        temp.setFirst_Name(x.getString("first_name"));
        temp.setLast_Name(x.getString("last_name"));
        temp.setEmail_address(x.getString("email"));
        temp.setObjectID(x.getObjectId());
        temp.setNumOfPunches(Integer.parseInt(x.getString("card_punches")));



        return temp;
    }

    @Override
    public void convertToInput(UserProfile x, ParseObject y) {



    }

    public void test(){
        Log.d("converter", "works");
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return converterBinder;
    }

    public class MyLocalConverterBinder extends Binder{
        ParseConverter getService(){
            return ParseConverter.this;
        }
    }


}