package com.xerorex.buvit;
import android.content.Context;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.text.Editable;
import android.text.TextWatcher;


import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class SearchUserActivity extends AppCompatActivity {

    private static final String TAG = "SearchUserActivity";
    private ParseConverter converter = new ParseConverter();
    private SearchEngine search = new SearchEngine();
    private UserProfile[] mainArray;
    private UserProfile userProfile = new UserProfile();
    private boolean isSorted = false;
    private EditText editSearchEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);


        getAllObjects();

        editSearchEntry = (EditText)findViewById(R.id.search_user_activity_search_bar);
        editSearchEntry.addTextChangedListener(theWatcher);

        configureSearchBar();

    }

    //Adds a hint to the search bar
    private void configureSearchBar() {

        EditText searchBar = (EditText) findViewById(R.id.search_user_activity_search_bar);
        searchBar.setHint("Search User...");
    }

    public void getAllObjects(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                parseToUserArray(objects);
            }


        });

    }

    public void parseToUserArray(List<ParseObject> list){

        mainArray = new UserProfile[list.size()];

        for(int i=0; i < list.size(); i++)
            mainArray[i] = new UserProfile();

        converter.test();

        converter.listToArray(list, mainArray);

        /*for(UserProfile i: mainArray)
            Log.d("It Works!",i.getFullName());*/

        populateFoundUsersList(mainArray);

    }



    //Creates the list view for the display and adds the content by using a passed arraylist
    private void populateFoundUsersList(UserProfile[] mainArray) {

        ListView foundUserList = (ListView) findViewById(R.id.search_user_activity_found_users);


        final ArrayList<UserProfile> allUsersList = new ArrayList<>();
        ArrayList<String> foundUsersList = new ArrayList<>();

        sort(mainArray);

        converter.userArrayToUserArrayList(mainArray, allUsersList);

        for(UserProfile i : allUsersList){
            String name = i.getFullName();
            foundUsersList.add(name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.found_users_list_content, foundUsersList);

        //Populates the list view with the userProfile ArrayList
        foundUserList.setAdapter(adapter);
        foundUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                userProfile = allUsersList.get(position);

                Intent startUserActvity = new Intent(getApplicationContext(), UserProfileActivity.class);
                startUserActvity.putExtra("ChosenUserProfile", userProfile);
                startActivity(startUserActvity);
            }
        });



    }

    private void populateFoundUsersList(UserProfile[] mainArray, ArrayList<UserProfile> narrowList) {

        ListView foundUserList = (ListView) findViewById(R.id.search_user_activity_found_users);


        final ArrayList<UserProfile> allUsersList = new ArrayList<>();
        ArrayList<String> foundUsersList = new ArrayList<>();

        sort(mainArray);

        converter.userArrayToUserArrayList(mainArray,allUsersList);

        for(UserProfile i : narrowList){
            String name = i.getFullName();
            foundUsersList.add(name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.found_users_list_content, foundUsersList);

        //Populates the list view with the userProfile ArrayList
        foundUserList.setAdapter(adapter);
        foundUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                userProfile = allUsersList.get(position);

                Intent startUserActvity = new Intent(getApplicationContext(), UserProfileActivity.class);
                startUserActvity.putExtra("ChosenUserProfile", userProfile);
                startActivity(startUserActvity);
            }
        });



    }

    private void sort(UserProfile[] array){
        if(isSorted == false) {
            search.sort(array);
            isSorted = true;
        }
        else
            Log.d(TAG,"Its already sorted");
    }

    private TextWatcher theWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable searchEntry) {

            Log.d(TAG, "textwatcher works");

            ArrayList<UserProfile> narrowList = new ArrayList<>();
            search.narrow(mainArray, narrowList, searchEntry.toString());

            populateFoundUsersList(mainArray,narrowList);
        }
    };

}