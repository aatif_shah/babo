package com.xerorex.buvit;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Binder;
import android.util.Log;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ProfileHandler extends Service {

    private final static String TAG = "ProfileHandler";

    private final IBinder profileBinder = new MyLocalBinder();

    public ProfileHandler() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return profileBinder;
    }





    public void addUserProfile(String first, String last, String email, int numOfPunches){
        ParseObject newProfile = new ParseObject("UserProfile");

        newProfile.put("first_name", first);
        newProfile.put("last_name", last);
        newProfile.put("email", email);
        newProfile.put("punches", numOfPunches);
        newProfile.saveInBackground();
    }

    public void addUserProfile(UserProfile newUser){
        ParseObject newProfile = new ParseObject("UserProfile");

        newProfile.put("first_name", newUser.getFirst_name());
        newProfile.put("last_name", newUser.getLast_name());
        newProfile.put("email_address", newUser.getEmail_address());
        newProfile.put("card_punches", String.valueOf(newUser.getNumOfPunches()));
/*        newProfile.put("email", newUser.getEmail_address());
        newProfile.put("punches", newUser.getNumOfPunches());*/
        newProfile.saveInBackground();
    }

    public void addUserProfile(ParseObject a){
        a.saveInBackground();
    }

    public void test(){

        Log.d("chicken","profilehandlerserviceworks");
    }

    public void removeUserProfile(UserProfile deletable){
        Log.d(TAG,deletable.getFirst_name());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");

        Log.d(TAG, deletable.getObjectID());

        query.getInBackground(deletable.getObjectID(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    // object will be your game score

                    try {
                        object.delete();
                    } catch (ParseException e1) {
                        Log.d(TAG, "could not delete");
                        e1.printStackTrace();
                    }
                } else {
                    e.printStackTrace();
                    // something went wrong
                }
            }
        });
    }

    public void updateUserProfile(final UserProfile updatable){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.getInBackground(updatable.getObjectID(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    // object will be your game score
                    Log.d(TAG,String.valueOf(updatable.getNumOfPunches()));
                    Log.d(TAG,updatable.getObjectID());
                    object.put("card_punches", String.valueOf(updatable.getNumOfPunches()));
                    object.saveInBackground();
                } else {
                    // something went wrong
                }
            }
        });
    }






    public class MyLocalBinder extends Binder{
        ProfileHandler getService(){

            return ProfileHandler.this;
        }
    }


}







