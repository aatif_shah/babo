package com.xerorex.buvit;

/**
 * Created by Aatif Shah on 11/24/2015.
 */
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Binder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


public class SearchEngine extends Service {

    private static final String TAG = "SearchEngine";

    private final IBinder searchEngineBinder = new MyLocalSearchBinder();
    ParseConverter convertor;

    private UserProfile[] array = new UserProfile[4];

    private List<ParseObject> allObjects = new ArrayList<>();




    public SearchEngine(){

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {return searchEngineBinder;}



    public void test(){
        Log.d(TAG, "searchengineserviceworks");
    }




/*    public void getAllObjects(){



        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                Log.d("got","dsssd");
                for(ParseObject i : objects) {
                    allObjects.add(i);
                    Log.d(TAG, i.getString("first_name"));
                }
            }


        });




    }

    public UserProfile[] getMainArray(){

        getAllObjects();

        Log.d(TAG,"inside getmainarray");

        UserProfile[] mainArray = new UserProfile[allObjects.size()+1];
        for(int i = 0; i < mainArray.length; i++)
            mainArray[i] = new UserProfile();

        convertor.listToArray(allObjects, mainArray);



        return mainArray;

    }*/






   /* public static void narrow(UserProfile[] a, String input){
        //sorts array
        QuickSort.quicksort(a);

        //returns all values that match with input
        System.out.println(slice(a, input));

    }*/

    public void sort(UserProfile[] array){
        QuickSort<UserProfile> sorter = new QuickSort<>();

        sorter.quicksort(array);
    }



/*    public ArrayList<UserProfile> getSortedMainList(){
        ArrayList<UserProfile> temp = new ArrayList<>();
        for(int i = 0; i < array.length; i++)
            temp.add(array[i]);
        return temp;
    }*/



    public static void narrow(UserProfile[] a, ArrayList<UserProfile> sliced, String input){

        for(int i = 0; i < a.length; i++){
            if(a[i].getFullName().substring(0, input.length()).equalsIgnoreCase(input))
                sliced.add(a[i]);

        }

    }


    public class MyLocalSearchBinder extends Binder{
        SearchEngine getService(){
            return SearchEngine.this; }
    }



}
