package com.xerorex.buvit;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Binder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Aatif Shah on 11/25/2015.*/


public class DataHandler extends Service {

    private List<ParseObject> parseObjects;


    IBinder dataBinder = new MyLocalDataBinder();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {return dataBinder;}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                parseObjects = objects;
            }


        });



        this.stopService(intent);

        return 0;
    }

    public List<ParseObject> getParseObjects(){

        return parseObjects;
    }

/*    public List<ParseObject> getCloudData(Runnable r){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                parseObjects = objects;
            }


        });
        return null
    }*/

    public void test(){
        Log.d("DataService","works");
    }


    public class MyLocalDataBinder extends Binder{
        DataHandler getService(){
            return DataHandler.this;
        }
    }

}
