package com.xerorex.buvit;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.parse.FindCallback;
import com.parse.ParseQuery;
import com.xerorex.buvit.ProfileHandler.MyLocalBinder;
import com.xerorex.buvit.ParseConverter.MyLocalConverterBinder;
import com.xerorex.buvit.DataHandler.MyLocalDataBinder;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

public class RemoveUserActivity extends AppCompatActivity {

    ProfileHandler profileService;
    ParseConverter converterService;
    DataHandler dataService;
    boolean dataServiceIsBound = false;
    boolean profileServiceIsBound = false;
    boolean converterServiceIsBound = false;
    private static final String TAG = "RemoveUserActivity";
    private UserProfile[] mainArray;
    private ArrayList<UserProfile> mainArrayList;
    private UserProfile userProfile = new UserProfile();
    private SearchEngine search = new SearchEngine();
    private boolean isSorted = false;
    EditText editSearchEntry;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_user);



        Intent profileIntent = new Intent(this, ProfileHandler.class);
        bindService(profileIntent, profileConnection,Context.BIND_AUTO_CREATE);

        Intent converterIntent = new Intent(this, ParseConverter.class);
        bindService(converterIntent,converterConnection,Context.BIND_AUTO_CREATE);

        Intent dataIntent = new Intent(this, DataHandler.class);
        bindService(dataIntent, dataConnection, Context.BIND_AUTO_CREATE);


        getAllObjects();

        editSearchEntry = (EditText)findViewById(R.id.remove_user_activity_search_bar);
        editSearchEntry.addTextChangedListener(theWatcher);



        configureView();

    }




    private TextWatcher theWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable searchEntry)
        {
            Log.d(TAG, "textwatcher works");

            ArrayList<UserProfile> narrowList = new ArrayList<>();
            search.narrow(mainArray, narrowList, searchEntry.toString());

            populateFoundUsersList(mainArray,narrowList);





        }
    };


    //Sets hint in search bar
    private void configureView() {
        //Search bar related methods

        EditText searchBar = (EditText) findViewById(R.id.remove_user_activity_search_bar);
        searchBar.setHint("Search User...");

    }

    public void getAllObjects(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                parseToUserArray(objects);
            }


        });

    }

    public void parseToUserList(List<ParseObject> list){
        converterService.listToArrayList(list, mainArrayList);
        for(UserProfile i : mainArrayList)
            Log.d("It Works!", i.getFullName());
    }



    public void parseToUserArray(List<ParseObject> list){

        mainArray = new UserProfile[list.size()];

        for(int i=0; i < list.size(); i++)
            mainArray[i] = new UserProfile();

        converterService.test();

        converterService.listToArray(list, mainArray);

        /*for(UserProfile i: mainArray)
            Log.d("It Works!",i.getFullName());*/

        populateFoundUsersList(mainArray);

    }

    //Shows the users available from passed arraylist
    private void populateFoundUsersList(UserProfile[] mainArray) {

        ListView foundUserList = (ListView) findViewById(R.id.remove_user_activity_found_users);


        final ArrayList<UserProfile> allUsersList = new ArrayList<>();
        ArrayList<String> foundUsersList = new ArrayList<>();



        sort(mainArray);

        converterService.userArrayToUserArrayList(mainArray, allUsersList);

        for(UserProfile i : allUsersList){
            String name = i.getFullName();
            foundUsersList.add(name);
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.found_users_list_content, foundUsersList);


        foundUserList.setAdapter(adapter);
        foundUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                userProfile = allUsersList.get(position);

                AlertDialog checkRemoveUser = AskOption(view);

                checkRemoveUser.show();


            }
        });



    }

    private void populateFoundUsersList(UserProfile[] mainArray,ArrayList<UserProfile> narrowList) {

        ListView foundUserList = (ListView) findViewById(R.id.remove_user_activity_found_users);


        final ArrayList<UserProfile> allUsersList = new ArrayList<>();
        ArrayList<String> foundUsersList = new ArrayList<>();



        sort(mainArray);

        converterService.userArrayToUserArrayList(mainArray, allUsersList);

        for(UserProfile i : narrowList){
            String name = i.getFullName();
            foundUsersList.add(name);
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.found_users_list_content, foundUsersList);


        foundUserList.setAdapter(adapter);
        foundUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                userProfile = allUsersList.get(position);

                AlertDialog checkRemoveUser = AskOption(view);

                checkRemoveUser.show();


            }
        });



    }

    //Creates AlertDialog box to confirm the removal of a userProfile
    private void sort(UserProfile[] array){
        if(isSorted == false) {
            search.sort(array);
            isSorted = true;
        }
        else
            Log.d(TAG,"Its already sorted");
    }
    private AlertDialog AskOption(View view)
    {

        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Remove User")
                .setMessage("Deleting user will remove the user from the store's user directory, press \"delete\" to confirm.")

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        Log.d(TAG,userProfile.getFullName());
                        profileService.removeUserProfile(userProfile);

                        Intent returnBack = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(returnBack);

                        dialog.dismiss();
                    }



                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }





    //SERVICE CONNECTIONS

    private ServiceConnection profileConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyLocalBinder profileBinder = (MyLocalBinder) service;
            profileService = profileBinder.getService();
            Log.d(TAG,"checkk");
            profileServiceIsBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            profileServiceIsBound = false;
        }
    };

    private ServiceConnection converterConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyLocalConverterBinder converterBinder = (MyLocalConverterBinder) service;
            converterService = converterBinder.getService();
            converterServiceIsBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            converterServiceIsBound = false;

        }
    };

    private ServiceConnection dataConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyLocalDataBinder dataBinder = (MyLocalDataBinder) service;
            dataService = dataBinder.getService();
            dataServiceIsBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            dataServiceIsBound = false;
        }
    };

}
